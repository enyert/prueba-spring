package com.synergygb.pruebaspring.model;

/**
 * Created by enyert on 24/11/15.
 */
public class Prueba {
    int id;
    String name;
    String description;

    public Prueba(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Prueba() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
