package com.synergygb.pruebaspring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by enyert on 24/11/15.
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.synergygb.pruebaspring")
public class PruebaConfiguration {
}
