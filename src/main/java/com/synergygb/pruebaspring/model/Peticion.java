package com.synergygb.pruebaspring.model;

/**
 * Created by enyert on 24/11/15.
 */
public class Peticion {
    int id;
    String message;

    public Peticion(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public Peticion() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
