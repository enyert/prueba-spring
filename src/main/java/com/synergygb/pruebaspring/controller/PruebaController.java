package com.synergygb.pruebaspring.controller;

import com.synergygb.pruebaspring.model.Peticion;
import com.synergygb.pruebaspring.model.Prueba;
import org.springframework.web.bind.annotation.*;

/**
 * Created by enyert on 24/11/15.
 */
@RestController
//Aqui debería ir un annotation @RequestMapping para la ruta base de un recurso
public class PruebaController {

    @RequestMapping(value="/prueba", method=RequestMethod.GET)
    public Prueba mostrarPrueba(@RequestBody Peticion input){
        Prueba prueba = new Prueba(1, input.getMessage(), "descripcion peticion");
        return prueba;
    }


    @RequestMapping(value="/prueba/{param}", method=RequestMethod.GET)
    public Prueba ejemploParam(@PathVariable String param){
        Prueba prueba = new Prueba(1, param, "algo");
        return prueba;
    }
}
